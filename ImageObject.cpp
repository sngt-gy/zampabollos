#include "header.h"
#include "templates.h"

#include "ImageObject.h"


ImageObject::ImageObject(void)
	:ImageSurface()
{
}


ImageObject::~ImageObject(void)
{
}

void ImageObject::SetColorKey(unsigned int color)
{
	SDL_SetColorKey(m_pSurface,SDL_SRCCOLORKEY, color);
}

void ImageObject::SetAlphaTransparent()
{
	m_pSurface = SDL_DisplayFormatAlpha(m_pSurface);
}