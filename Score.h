#pragma once
#include "displayableobject.h"
class Score :
	public DisplayableObject
{
public:
	Score(BaseEngine* pEngine, int startX, int startY);
	~Score(void);

	void Draw(void);
	void DoUpdate(int iCurrentTime);
	
	void SetScore (int score);
	void SetLevel (int lvl);

private:
	int score;
	int level;
};

