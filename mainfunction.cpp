#include "header.h"
#include "templates.h"

#include "Stage.h"

// Needs one of the following #includes, to include the class definition
#define BASE_SCREEN_WIDTH 450
#define BASE_SCREEN_HEIGHT 640

int main(int argc, char *argv[])
{
	int iResult;
	Stage oMain;

	char buf[1024];
	sprintf( buf, "El Zampabollos", BASE_SCREEN_WIDTH, BASE_SCREEN_HEIGHT );
	iResult = oMain.Initialise( buf, BASE_SCREEN_WIDTH, BASE_SCREEN_HEIGHT, "assets/dotdigital.ttf", 16 );
	iResult = oMain.MainLoop();
	oMain.Deinitialise();

	return iResult;
}
