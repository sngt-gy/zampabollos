#include "header.h"
#include "templates.h"

#include "HeroObject.h"
#include "PebbleObject.h"

const int HeroObject::HERO_WIDTH   = 72;
const int HeroObject::HERO_HEIGHT  = 76;
const int HeroObject::HERO_Dead = -1;
const int HeroObject::HERO_Idle = 0;
const int HeroObject::HERO_Landing = 1;
const int HeroObject::HERO_Flying  = 2;

HeroObject::HeroObject(BaseEngine* pEngine, int startX, int startY)
	:DisplayableObject( pEngine )
	,timeFix(0)	,timer(0), status(HERO_Idle), idleTime(0), xMovement(0), yMovement(0)
	, highJump(false)
{
	int initialDrop = 80;

	m_iDrawWidth = HERO_WIDTH;
	m_iDrawHeight = HERO_HEIGHT;
	
	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	m_iPreviousScreenX = m_iCurrentScreenX = startX;
	m_iPreviousScreenY = m_iCurrentScreenY = startY - m_iDrawHeight - initialDrop;

	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenX;
	box.w = m_iDrawWidth;
	box.h = m_iDrawHeight;

	collision.x = 15;
	collision.y = 15;
	collision.w = 42;
	collision.h = 56;

	bool h;
	image = new ImageObject();
	h = image->LoadImage("assets/zampa.png");
	
	if(h) 
	{
		//printf("Image Loaded Man!");
		image->SetAlphaTransparent();
		
	}

	SetVisible(true);
}


HeroObject::~HeroObject()
{
}

void HeroObject::Draw() 
{
	/*GetEngine()->DrawScreenOval(
		m_iCurrentScreenX, m_iCurrentScreenY,
		m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
		0xffccdd
	);*/

	image->RenderImage(GetEngine()->GetSDLSurface(),0,0,m_iCurrentScreenX, m_iCurrentScreenY, HERO_WIDTH, HERO_HEIGHT);
	StoreLastScreenPositionAndUpdateRect();
	
	SetVisible(false);
}

void HeroObject::DoUpdate(int iCurrentTime) 
{
	if( status == HERO_Dead) 
	{
		m_iCurrentScreenY++;
	}
	else {

		if(timer > 0) {
			if (iCurrentTime > timer+idleTime) {
				highJump = false;
			
				timeFix = iCurrentTime;
				timer = 0;
			} else {
				status = HERO_Idle;
				return;
			}
		}

		//Key Presses
		KeyActions();

		//Vertical Movement
		//formula that makes the user bounce
		int frameDuration = 100, gravity = 2; 
		int initialSpeed = 850;
		if(highJump)
			initialSpeed = 3000;
	
		yMovement = (initialSpeed - gravity * (iCurrentTime - timeFix)) / frameDuration;

		if(yMovement < 0)
			status = HERO_Landing;
		else if (yMovement > 0)
			status = HERO_Flying;

		m_iCurrentScreenY -= yMovement;

		// Horizontal Movement - 
		// values are changed according to key presses - KeyActions()
		float friction = 0.05;
		m_iCurrentScreenX += xMovement; // Move hero horizontally if there are any key presses
		//Apply friction to horizontal movement
		if(xMovement < friction) {
			xMovement += friction;
		} else if (xMovement > -friction) {
			xMovement -= friction;
		}
		
		// Teleport to the opposite screen edge
		if (m_iCurrentScreenX + m_iDrawWidth - 10 < 0) {
			m_iCurrentScreenX = GetEngine()->GetScreenWidth() - m_iDrawWidth;
		} else if (m_iCurrentScreenX > GetEngine()->GetScreenWidth()) {
			m_iCurrentScreenX = 0+10;
		}
	}

	RedrawObjects();
}

void HeroObject::KeyActions() {
	//values for when hero moves horizontally
	float xSpeed = 0.3, xSpeedLimit = xSpeed * 10;

	if(GetEngine()->IsKeyPressed(SDLK_LEFT)) {
		//m_iCurrentScreenX -= 2;
		if(xMovement > 0)
			xMovement = 0;
		if(xMovement > -xSpeedLimit)
			xMovement -= xSpeed; // increase speed 
	}

	if(GetEngine()->IsKeyPressed(SDLK_RIGHT)) {
		//m_iCurrentScreenX += 2;
		if(xMovement < 0) 
			xMovement = 0;
		if(xMovement < xSpeedLimit)
			xMovement += xSpeed; // increase speed
			
	}

	if(GetEngine()->IsKeyPressed(SDLK_SPACE)) {
		if( status == HERO_Idle || status == HERO_Flying)
			highJump = true;
			
	}
}

void HeroObject::SetJumpingBase(PebbleObject* pebble) 
{
	timer = GetEngine()->GetModifiedTime();
}

void HeroObject::Scroll(int speed) 
{
	m_iCurrentScreenY += speed;
}

void HeroObject::SetStatus(int nStatus)
{
	status = nStatus;
}

int HeroObject::GetStatus()
{
	return status;
}

bool HeroObject::IsSuperJump()
{
	return highJump;
}

SDL_Rect HeroObject::GetCollisionBox(void)
{
	SDL_Rect b = collision;
	b.x +=  m_iCurrentScreenX; // current x + collision offset
	b.y += m_iCurrentScreenY; // current y + collision offset
	return b;
}

SDL_Rect HeroObject::GetBox(void) 
{
	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	return box;
}
