#include "header.h"
#include "templates.h"

#include "MeteorObject.h"


MeteorObject::MeteorObject(BaseEngine* pEngine, int id, int startX, int startY)
	: DisplayableObject(pEngine)
	, ID(id)
{
	m_iDrawWidth = 182;
	m_iDrawHeight = 183;

	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	m_iPreviousScreenX = m_iCurrentScreenX = startX;
	m_iPreviousScreenY = m_iCurrentScreenY = startY;
	
	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	box.w = m_iDrawWidth;
	box.h = m_iDrawHeight;

	collision = box;

	bool h;
	image = new ImageObject();
	h = image->LoadImage("assets/meteor.png");
	
	if(h) 
	{
		printf("Meteor Object Loaded \n");
		image->SetAlphaTransparent();
	}


	SetVisible(true);
}


MeteorObject::~MeteorObject(void)
{
}


void MeteorObject::Draw() 
{
	//GetEngine()->DrawScreenRectangle(
	//	m_iCurrentScreenX, m_iCurrentScreenY, 
	//	m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
	//	0x009911
	//);

	image->RenderImage(GetEngine()->GetSDLSurface(),0,0,m_iCurrentScreenX, m_iCurrentScreenY, m_iDrawWidth, m_iDrawHeight);

	/* Debug
	char buf[40];
	sprintf(buf, "METEORR: %d", ID);

	GetEngine()->DrawScreenString(
		m_iCurrentScreenX, m_iCurrentScreenY,
		buf, 0x000099,
		GetEngine()->GetFont("assets/FreeSerif.ttf", 18)
	);
	*/
	StoreLastScreenPositionAndUpdateRect();
}

void MeteorObject::DoUpdate(int iCurrentTime) 
{
	RedrawObjects();
}

void MeteorObject::Scroll(int speed) 
{
	m_iCurrentScreenY += speed;
}

void MeteorObject::SetPosition(int x, int y) 
{
	m_iCurrentScreenX = x;
	m_iCurrentScreenY = y;
}


SDL_Rect MeteorObject::GetCollisionBox() 
{
	collision.x = m_iCurrentScreenX;
	collision.y = m_iCurrentScreenY;
	return collision;
}

SDL_Rect MeteorObject::GetBox() 
{
	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	return box;
}