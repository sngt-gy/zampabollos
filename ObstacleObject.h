#pragma once

#include "displayableobject.h"
#include "MovementPosition.h"

class ObstacleObject :
	public DisplayableObject
{
public:
	ObstacleObject(BaseEngine* pEngine, int startX, int startY);
	~ObstacleObject(void);

	void Draw();
	void DoUpdate(int iCurrentTime);
	void SetMovement( int iStartTime, int iEndTime, int iCurrentTime,
				int iStartX, int iStartY, int iEndX, int iEndY );
	void StartMovement(SDL_Rect iPosition);
	void StopMovement();

	void Scroll(int speed);

	void SetPosition(int x, int y);


	SDL_Rect GetCollisionBox(void);
	SDL_Rect GetBox(void);

private:
	int positionOffsetY;
	int movementOffset;
	int movementDuration;

	MovementPosition m_oMovement;
	SDL_Rect collision;
	SDL_Rect box;
};

