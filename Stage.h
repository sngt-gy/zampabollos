#pragma once
#include "baseengine.h"

#include "HeroObject.h"
#include "PebbleObject.h"
#include "Score.h"
#include "Background.h"
#include "MeteorObject.h"
#include "ObstacleObject.h"

class Stage :
	public BaseEngine
{
public:
	Stage(void);
	~Stage(void);
	void SetupBackgroundBuffer(void);
	int InitialiseObjects(void);	
	void GameAction(void);	
	void KeyDown(int iKeyCode);

	void HeroAction(void);
	bool HeroPebbleCollision(SDL_Rect hero, SDL_Rect pebble);
	void ScrollScreen(void);
	void ProcessPebbles(void);
	void ProcessMeteors(void);
	void ProcessBgObjects(void);
	void ProcessObstacles(void);
	void UpdateScore(void);

	SDL_Rect PlacePebble(void);
	SDL_Rect PlaceMeteor(void);

	int GetModifiedTime(void);


private:
	bool pause;
	int m_iTick;
	int score;
	int level;
	int pebCount;
	int meteorCount;
	int obsCount;

	bool isMovingPreviousPebble;
	int activePebbles;
	int movingPebbles;
	int maxMovingPebbles;

	Score* scoreBoard;
	HeroObject* hero;
	SDL_Rect pebbleGap;
	
	PebbleObject** pebbles;
	PebbleObject* pebbleJumpingBase;
	PebbleObject* topMostPebble;
	Background** mainBg;

	MeteorObject* topMostMeteor;
	MeteorObject** meteors;
	
	ObstacleObject** obstacles;
	
};

