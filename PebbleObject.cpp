#include "header.h"
#include "templates.h"

#include "PebbleObject.h"
#include "MovementPosition.h"

const int PebbleObject::PEBBLE_WIDTH = 89;
const int PebbleObject::PEBBLE_HEIGHT = 24;

PebbleObject::PebbleObject(BaseEngine* pEngine, int id, int startX, int startY)
	:DisplayableObject(pEngine)
	, ID(id)
	, moveH(false)
	, movementDuration(3000)
{
	m_iDrawWidth = PEBBLE_WIDTH;
	m_iDrawHeight = PEBBLE_HEIGHT;

	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	m_iPreviousScreenX = m_iCurrentScreenX = startX;
	m_iPreviousScreenY = m_iCurrentScreenY = startY;

	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	box.w = m_iDrawWidth;
	box.h = m_iDrawHeight;

	collision.x = 2;
	collision.y = 2;
	collision.h = 10;
	collision.w = box.w - 4;

	bool h;
	image = new ImageObject();
	h = image->LoadImage("assets/pebble.png");
	
	if(h) 
	{
		//printf("Image Loaded Man!");
		image->SetAlphaTransparent();

	}


	SetVisible(true);
}


PebbleObject::~PebbleObject(void)
{
}


void PebbleObject::Draw() 
{
	//GetEngine()->DrawScreenRectangle(
	//	m_iCurrentScreenX, m_iCurrentScreenY, 
	//	m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
	//	0x009911
	//);

	image->RenderImage(GetEngine()->GetSDLSurface(),0,0,m_iCurrentScreenX, m_iCurrentScreenY, PEBBLE_WIDTH,PEBBLE_HEIGHT);
	StoreLastScreenPositionAndUpdateRect();
}

void PebbleObject::DoUpdate(int iCurrentTime) 
{
	if(moveH) 
	{
		// Work out current position
		m_oMovement.Calculate(iCurrentTime);
		m_iCurrentScreenX = m_oMovement.GetX();
		

		if(m_oMovement.HasMovementFinished(iCurrentTime))
		{
			m_oMovement.Reverse();
			m_oMovement.Calculate(iCurrentTime);
			m_iCurrentScreenX = m_oMovement.GetX();
		}
	}
	RedrawObjects();
}

void PebbleObject::Scroll(int speed) 
{
	m_iCurrentScreenY += speed;
}

void PebbleObject::SetMovement( 
	int iStartTime, int iEndTime, int iCurrentTime,
	int iStartX, int iStartY, int iEndX, int iEndY ) 
{
	m_oMovement.Setup( iStartX, iStartY, iEndX, iEndY, iStartTime, iEndTime );
	m_oMovement.Calculate( iCurrentTime );
	m_iCurrentScreenX = m_oMovement.GetX();
}

void PebbleObject::StartMovement()
{
	int startX = 0,
	endX = GetEngine()->GetScreenWidth() - m_iDrawWidth,
	startY = m_iCurrentScreenY,
	endY = m_iCurrentScreenY,
	startTime = GetEngine()->GetModifiedTime(),
	endTime = startTime + movementDuration;

	m_oMovement.Setup(startX, startY, endX, endY, startTime, endTime);
	moveH = true;
}

void PebbleObject::StopMovement()
{
	moveH = false;
}

bool PebbleObject::IsMoving()
{
	return moveH;
}

void PebbleObject::SetPosition(int x, int y) 
{
	m_iCurrentScreenX = x;
	m_iCurrentScreenY = y;
}

int PebbleObject::GetID() 
{
	return ID;
}

SDL_Rect PebbleObject::GetCollisionBox() 
{
	SDL_Rect b = collision;

	b.x +=  m_iCurrentScreenX; // current x + collision offset
	b.y += m_iCurrentScreenY; // current y + collision offset
	return b;
}

SDL_Rect PebbleObject::GetBox() 
{
	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	return box;
}