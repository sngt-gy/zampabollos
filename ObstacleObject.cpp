#include "header.h"
#include "templates.h"

#include "ObstacleObject.h"
#include <math.h>


ObstacleObject::ObstacleObject(BaseEngine* pEngine, int startX, int startY)
	: DisplayableObject(pEngine)
	, positionOffsetY(40)
	, movementOffset(100)
	, movementDuration(3000)
{
	m_iDrawWidth = 20;
	m_iDrawHeight = 20;

	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	m_iPreviousScreenX = m_iCurrentScreenX = startX;
	m_iPreviousScreenY = m_iCurrentScreenY = startY;

	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	box.w = m_iDrawWidth;
	box.h = m_iDrawHeight;

	collision = box;
	
	SetVisible(false);
}


ObstacleObject::~ObstacleObject(void)
{
}

void ObstacleObject::Draw() 
{
	if(IsVisible())
	{
		GetEngine()->DrawScreenRectangle(
			m_iCurrentScreenX, m_iCurrentScreenY, 
			m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
			0x009911
		);
		//image->RenderImage(GetEngine()->GetSDLSurface(),0,0,m_iCurrentScreenX, m_iCurrentScreenY, PEBBLE_WIDTH,PEBBLE_HEIGHT);
		StoreLastScreenPositionAndUpdateRect();
	}
}

void ObstacleObject::DoUpdate(int iCurrentTime) 
{
	if(IsVisible())
	{
		int amp = 15; 
		float frequency = 0.01;
		float oscillation = 2 * amp 
				 * sin( frequency * ( 2 * iCurrentTime + 10 ) / 2 ) 
				 * sin(5 * frequency);
		m_iCurrentScreenY += floor( oscillation + 0.5);

		// Work out current position
		m_oMovement.Calculate(iCurrentTime);
		m_iCurrentScreenX = m_oMovement.GetX();
		

		if(m_oMovement.HasMovementFinished(iCurrentTime))
		{
			m_oMovement.Reverse();
			m_oMovement.Calculate(iCurrentTime);
			m_iCurrentScreenX = m_oMovement.GetX();
		}
		RedrawObjects();
	}
}

void ObstacleObject::SetMovement( 
	int iStartTime, int iEndTime, int iCurrentTime,
	int iStartX, int iStartY, int iEndX, int iEndY ) 
{
	m_oMovement.Setup( iStartX, iStartY, iEndX, iEndY, iStartTime, iEndTime );
	m_oMovement.Calculate( iCurrentTime );
	m_iCurrentScreenX = m_oMovement.GetX();
}

void ObstacleObject::StartMovement(SDL_Rect iPosition)
{
	m_iCurrentScreenY= iPosition.y - positionOffsetY;

	int startX = GetEngine()->GetScreenWidth() - m_iDrawWidth,
		endX = 0,
		startY = m_iCurrentScreenY,
		endY = m_iCurrentScreenY,
		startTime = GetEngine()->GetModifiedTime(),
		endTime = startTime + movementDuration;

	m_oMovement.Setup(startX, startY, endX, endY, startTime, endTime);
	SetVisible(true);
}

void ObstacleObject::StopMovement()
{
	SetVisible(false);
}

void ObstacleObject::Scroll(int speed) 
{
	m_iCurrentScreenY += speed;
}

void ObstacleObject::SetPosition(int x, int y) 
{
	m_iCurrentScreenX = x;
	m_iCurrentScreenY = y;
}

SDL_Rect ObstacleObject::GetCollisionBox() 
{
	collision.x = m_iCurrentScreenX;
	collision.y = m_iCurrentScreenY;
	return collision;
}

SDL_Rect ObstacleObject::GetBox() 
{
	box.x = m_iCurrentScreenX;
	box.y = m_iCurrentScreenY;
	return box;
}