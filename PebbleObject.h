#pragma once
#include "displayableobject.h"
#include "ImageObject.h"
#include "MovementPosition.h"
class PebbleObject :
	public DisplayableObject
{
public:
	static const int PEBBLE_WIDTH;
	static const int PEBBLE_HEIGHT;

	PebbleObject(BaseEngine* pEngine, int id, int startX, int startY);
	~PebbleObject(void);

	void Draw();
	void DoUpdate(int iCurrentTime);
	void Scroll(int speed);

	void SetMovement( int iStartTime, int iEndTime, int iCurrentTime,
				int iStartX, int iStartY, int iEndX, int iEndY );

	void StartMovement();
	void StopMovement();
	bool IsMoving();

	void SetPosition(int x, int y);
	int GetID(void);

	SDL_Rect GetCollisionBox(void);
	SDL_Rect GetBox(void);

private:
	int ID;
	int movementDuration;
	bool moveH;

	MovementPosition m_oMovement;
	SDL_Rect collision;
	SDL_Rect box;
	ImageObject* image;
};

