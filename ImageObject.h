#pragma once
#include "jpgimage.h"
class ImageObject :
	public ImageSurface
{
public:
	ImageObject(void);
	~ImageObject(void);

	void SetColorKey(unsigned int color);
	void SetAlphaTransparent(void);
};

