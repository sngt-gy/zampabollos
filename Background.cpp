#include "header.h"
#include "templates.h"

#include "Background.h"


Background::Background(BaseEngine* pEngine)
	:DisplayableObject(pEngine)
{
	m_iDrawWidth = pEngine->GetScreenWidth();
	m_iDrawHeight = pEngine->GetScreenHeight();

	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	m_iPreviousScreenX = m_iCurrentScreenX = 0;
	m_iPreviousScreenY = m_iCurrentScreenY = 0;

	bool h;
	image = new ImageObject();
	h = image->LoadImage("assets/main-bg.jpg");
	
	if(!h) 
	{
		printf("Main background image not loaded");
	}

	SetVisible(true);
}


Background::~Background(void)
{
}

void Background::Draw() 
{
	/*GetEngine()->DrawScreenOval(
		m_iCurrentScreenX, m_iCurrentScreenY,
		m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
		0xffccdd
	);*/

	image->RenderImage(GetEngine()->GetSDLSurface(),0,0,m_iCurrentScreenX, m_iCurrentScreenY, m_iDrawWidth, m_iDrawHeight);
	StoreLastScreenPositionAndUpdateRect();
}


void Background::DoUpdate(int iCurrentTime)
{
	RedrawObjects();
}

void Background::Scroll(int speed) 
{
	m_iCurrentScreenY += speed;
}


void Background::SetPosition(int x, int y) 
{
	m_iCurrentScreenX = x;
	m_iCurrentScreenY = y;
}

int Background::GetPosY()
{
	return m_iCurrentScreenY;
}