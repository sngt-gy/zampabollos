#include "header.h"
#include "templates.h"

#include "Score.h"


Score::Score(BaseEngine* pEngine, int startX, int startY)
	:DisplayableObject(pEngine), score(0), level(0)
{
	m_iPreviousScreenX = m_iCurrentScreenX = 0;
	m_iPreviousScreenY = m_iCurrentScreenY = 0;
	
	m_iDrawHeight = 20;
	m_iDrawWidth = GetEngine()->GetScreenWidth();

	m_iStartDrawPosX = m_iStartDrawPosY = 0;

	SetVisible(true);
}


Score::~Score(void)
{
}

void Score::Draw()
{
	GetEngine()->DrawScreenRectangle(
		m_iCurrentScreenX, m_iCurrentScreenY,
		m_iCurrentScreenX + m_iDrawWidth - 1, m_iCurrentScreenY + m_iDrawHeight - 1,
		0x0099cc
	);
	
	char buf[40];
	sprintf(buf, "Level: %d, Score: %d", level, score);

	GetEngine()->DrawScreenString(
		m_iCurrentScreenX, m_iCurrentScreenY,
		buf, 0xddeeff,
		GetEngine()->GetFont("assets/FreeSerif.ttf", 18)
	);

	StoreLastScreenPositionAndUpdateRect();

}

void Score::DoUpdate(int iCurrentTime)
{
	RedrawObjects();
}
	
void Score::SetScore (int score)
{
	this->score = score;
}

void Score::SetLevel (int lvl)
{
	level = lvl;
}
