#include "header.h"
#include "templates.h"
#include <ctime>

#include "Stage.h"
#include "PebbleObject.h"
#include "HeroObject.h"
#include "JPGImage.h"
#include "Background.h"
#include "MeteorObject.h"
#include "ObstacleObject.h"

Stage::Stage(void)
	:BaseEngine( 6 )
	, pebCount(25), meteorCount(6), obsCount(1)
	, level(1), score(0)
	, isMovingPreviousPebble(false)
	, activePebbles(0)
	, movingPebbles(0)
	, maxMovingPebbles(2)
	, pause(false), m_iTick(0)
{
}


Stage::~Stage(void)
{
}

void Stage::SetupBackgroundBuffer() 
{
	FillBackground(0xf5f0db);
}

int Stage::InitialiseObjects()
{
	DrawableObjectsChanged();
	DestroyOldObjects();

	//Set up pebble gaps 
	int maxGapX = GetScreenWidth()-PebbleObject::PEBBLE_WIDTH;
	int maxGapY = 200;

	pebbleGap.x = GetScreenWidth()/2;
	pebbleGap.y = GetScreenHeight();
	pebbleGap.w = maxGapX;
	pebbleGap.h = maxGapY;

	topMostMeteor = new MeteorObject(this, 0, 0, pebbleGap.y);
	topMostPebble = new PebbleObject(this, 0, pebbleGap.x, pebbleGap.y);

	// Array size: Pebbles + Meteor 2bg + Hero + Score  + End NULL element
	int bgCount = 2;
	int objectCount = pebCount + meteorCount + bgCount + 1 + 1 + 1;
	m_ppDisplayableObjects = new DisplayableObject*[objectCount];

	//Backgrounds
	mainBg = new Background*[2];
	mainBg[0] = new Background(this);
	mainBg[1] = new Background(this);
	mainBg[1]->SetPosition(0, 0 - GetScreenHeight());

	m_ppDisplayableObjects[0] = mainBg[0];
	m_ppDisplayableObjects[1] = mainBg[1];
	
	pebbles = new PebbleObject*[pebCount];	
	meteors = new MeteorObject*[meteorCount];
	obstacles = new ObstacleObject*[obsCount];

	SDL_Rect location;
	srand((unsigned)time(0)); 

	for (int i = 0; i < meteorCount; i++) 
	{
		location = PlaceMeteor();
		
		meteors[i] = new MeteorObject(this,i, location.x, location.y);
		m_ppDisplayableObjects[i+bgCount] = meteors[i]; 
		topMostMeteor = meteors[i];
	}

	for (int i = 0; i < pebCount; i++) 
	{
		location = PlacePebble();
		pebbles[i] = new PebbleObject(
			this, i, 
			location.x, location.y
		); 
		m_ppDisplayableObjects[i+meteorCount+bgCount] = pebbles[i]; 
		topMostPebble = pebbles[i];
	}

	for (int i = 0; i < obsCount; i++) 
	{
		obstacles[i] = new ObstacleObject(this, 100, 100);
		m_ppDisplayableObjects[i+meteorCount+pebCount+bgCount] = obstacles[i]; 
		
	}


	// set our initial jumping base as the first pebble
	pebbleJumpingBase = pebbles[0];

	// Create and place our Hero to the screen
	hero = new HeroObject(this, pebbleJumpingBase->GetBox().x, pebbleJumpingBase->GetBox().y);
	hero->SetJumpingBase(pebbleJumpingBase);
	m_ppDisplayableObjects[meteorCount+bgCount+pebCount+obsCount] = hero;


	// Score Object 
	scoreBoard = new Score(this,0,0);
	m_ppDisplayableObjects[meteorCount+bgCount+pebCount+obsCount+1] = scoreBoard;

	//Add last null item
	m_ppDisplayableObjects[meteorCount+bgCount+pebCount+obsCount+2] = NULL;
	return 0;
}


void Stage::GameAction() 
{
	// If too early to act then do nothing
	if ( !TimeToAct() || pause)
		return;
	
	// Don't act for another 1 tick
	SetTimeToAct( 10 );
	m_iTick += 10; // Update time

	// Tell all objects to update themselves
	UpdateAllObjects( GetModifiedTime() );

	//Actions for Hero
	HeroAction();
	ScrollScreen();
	ProcessPebbles();
	ProcessMeteors();
	ProcessBgObjects();
	if(level>5)
		ProcessObstacles();


}

void Stage::KeyDown(int iKeyCode)
{
	switch (iKeyCode)
	{
	case SDLK_p:
		pause = !pause;
		break;
	}
}

void Stage::HeroAction() 
{
	for (int i = 0; i < pebCount; i++)
	{
		if( HeroPebbleCollision(hero->GetCollisionBox(), pebbles[i]->GetCollisionBox()) 
			&& hero->GetStatus() == hero->HERO_Landing ) 
		{
			//printf("Collision");
			pebbleJumpingBase = pebbles[i];
			hero->SetJumpingBase(pebbleJumpingBase);
		}
	}

	for (int i = 0; i < obsCount; i++)
	{
		if( HeroPebbleCollision(hero->GetCollisionBox(), obstacles[i]->GetCollisionBox()) ) 
		{
			//printf("Obstacle -- Deadd!!!");
			hero->SetStatus(hero->HERO_Dead);
		}
	}
}

bool Stage::HeroPebbleCollision(SDL_Rect hero, SDL_Rect pebble) 
{
	if (hero.x+hero.w <= pebble.x)
		return false;
	if (hero.x >= pebble.x+pebble.w) 
		return false;
	if (hero.y+hero.h <= pebble.y)
		return false;
	if (hero.y+hero.h >= pebble.y+pebble.h)
		return false;

	return true;
}

void Stage::ScrollScreen(void) 
{
	int heroY = hero->GetBox().y + hero->GetBox().h;
	int speed = 0;
	int	jumpingBaseThreshold = GetScreenHeight() * 0.85;

	if ( pebbleJumpingBase->GetBox().y < jumpingBaseThreshold
		|| heroY < GetScreenHeight() * 0.3) 
	{
		int offset = (jumpingBaseThreshold - heroY);
		speed = (offset * offset) / 5000;
		//speed = 1;
		if(speed > 1) {
			mainBg[0]->Scroll(speed * 0.5);
			mainBg[1]->Scroll(speed * 0.5);

		
			for (int i = 0; i < meteorCount; i++)
			{
				meteors[i]->Scroll(speed * 0.7);
			}

			for (int i = 0; i < pebCount; i++)
			{
				pebbles[i]->Scroll(speed);
			}

			for (int i = 0; i < obsCount; i++)
			{
				if(obstacles[i]->IsVisible())
				{
					obstacles[i]->Scroll(speed);
				}
			}


			hero->Scroll(speed);
			
		
			UpdateScore();//update score
		}
	}
}

SDL_Rect Stage::PlacePebble()
{
	// Place the pebbles a radom place within a range
	int minY = pebbleGap.h * 0.8;
	int maxY = pebbleGap.h;
	float constant =  1 - (1/ ( ((float) level/4) +1) );


	int minX = 1, maxX = pebbleGap.w; // Horizontal Range
	int x = 1, y = 0;

	x = rand()% maxX + minX;
	y = rand()%(maxY - minY) + minY;
	y *= constant;

	y = topMostPebble->GetBox().y - y;
	SDL_Rect box = {x,y,0,0};
	
	return box;
}

SDL_Rect Stage::PlaceMeteor()
{
	// Place the meteors a radom place within a range
	int minY = 50;
	int maxY = 100;

	int minX = 1, maxX = GetScreenWidth() - 40; // Horizontal Range
	int x = 1, y = 0;

	x = rand()% maxX + minX;
	y = rand()%(maxY - minY) + minY;

	y = ( topMostMeteor->GetBox().y - topMostMeteor->GetBox().h ) - y;

	SDL_Rect box = {x,y,0,0};

	//printf("top meteor %d\n", y);
	return box;
}


void Stage::ProcessPebbles() 
{
	SDL_Rect location;
	for (int i = 0; i < pebCount; i++)
	{

		if(pebbles[i]->GetBox().y <  0-pebbles[i]->GetBox().h)
		{
			if( !pebbles[i]->IsMoving() && !isMovingPreviousPebble	&& movingPebbles <= maxMovingPebbles ) {
				int random = rand() % 5;
				if(random == 0) {
					pebbles[i]->StartMovement();
					movingPebbles++;
					isMovingPreviousPebble = true;
				}
			}
		} 
		else if(pebbles[i]->GetBox().y > GetScreenHeight()) 
		{
			if (pebbles[i]->IsMoving()) {
				pebbles[i]->StopMovement();
				isMovingPreviousPebble = false;
				movingPebbles--;
			}

			// Re-incarnate the pebbles 
			location = PlacePebble();
			pebbles[i]->SetPosition(location.x, location.y);
			topMostPebble = pebbles[i];
		}
		
	}

}

void Stage::ProcessMeteors() 
{
	SDL_Rect location;
	for (int i = 0; i < meteorCount; i++)
	{
		// Re-incarnate the meteors 
		if(meteors[i]->GetBox().y > GetScreenHeight()) 
		{
			location = PlaceMeteor();
			meteors[i]->SetPosition(location.x, location.y);
			topMostMeteor = meteors[i];
		}
	}

}



void Stage::ProcessObstacles() 
{
	for (int i = 0; i < obsCount; i++)
	{
		if(!obstacles[i]->IsVisible())
		{
			SDL_Rect iPos = topMostPebble->GetBox();
			iPos.x = topMostPebble->GetXCentre(); 
			obstacles[i]->StartMovement(iPos);

		} else {
			if(obstacles[i]->GetBox().y > GetScreenHeight())
			{
				obstacles[i]->StopMovement();
			}
		}
	}

}


void Stage::ProcessBgObjects()
{
	if(mainBg[0]->GetPosY() > GetScreenHeight())
		mainBg[0]->SetPosition(0, mainBg[1]->GetPosY()-GetScreenHeight());
	if(mainBg[1]->GetPosY() > GetScreenHeight())
		mainBg[1]->SetPosition(0, mainBg[0]->GetPosY()-GetScreenHeight());
}

void Stage::UpdateScore() 
{
	score++;
	if(score%150 == 0)
		level++;
	scoreBoard->SetScore(score);
	scoreBoard->SetLevel(level);
}

int Stage::GetModifiedTime()
{
	return m_iTick;
}