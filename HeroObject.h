#pragma once
#include "displayableobject.h"
#include "PebbleObject.h"
#include "ImageObject.h"

class HeroObject :
	public DisplayableObject
{
public:
	static const int HERO_WIDTH;
	static const int HERO_HEIGHT;
	static const int HERO_Landing;
	static const int HERO_Idle;
	static const int HERO_Flying;
	static const int HERO_Dead;

	HeroObject(BaseEngine* pEngine, int startX, int startY);
	~HeroObject(void);

	void Draw(void);
	void DoUpdate(int iCurrentTime);
	void KeyActions(void);
	void SetJumpingBase(PebbleObject* pebble);

	void Scroll(int speed);
	void SetStatus(int nStatus);
	int GetStatus(void);

	bool IsSuperJump();

	SDL_Rect GetCollisionBox(void);
	SDL_Rect GetBox(void);

private: 
	bool highJump;

	SDL_Rect collision;
	SDL_Rect box;

	int status;
	int timeFix;
	int timer;
	int idleTime;
	float xMovement;
	int yMovement;

	ImageObject* image;
};

