#pragma once
#include "displayableobject.h"
#include "ImageObject.h"

class Background :
	public DisplayableObject
{
public:
	Background(BaseEngine* pEnginge);
	~Background(void);

	void Draw(void);
	void DoUpdate(int iCurrentTime);

	void Scroll(int speed);
	void SetPosition(int x, int y);

	int GetPosY (void);

private:
	ImageObject* image;
};

