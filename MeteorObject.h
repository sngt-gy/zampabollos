#pragma once
#include "displayableobject.h"
#include "ImageObject.h"

class MeteorObject :
	public DisplayableObject
{
public:
	MeteorObject(BaseEngine* pEngine, int id, int startX, int startY);
	~MeteorObject(void);
	void Draw();
	void DoUpdate(int iCurrentTime);
	void Scroll(int speed);
	void SetPosition(int x, int y);
	
	SDL_Rect GetCollisionBox(void);
	SDL_Rect GetBox(void);

private:
	int ID;
	SDL_Rect collision;
	SDL_Rect box;
	ImageObject* image;
};

